package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio17 {

	public static void executar(Scanner leitor) {

		String s = "Ex 17. Elabore um programa que receba três notas de um aluno os pesos\r\n"
				+ "referentes a cada nota e retorne a sua média ponderada.\\r\\n"
				+ "Cálculo da média ponderada:\r\n\r\n"
				+ "media = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 +\r\n" + "peso3)\r\n\r\n"
				+ "Exemplo: nota1 = 10.0, nota2 = 5.5, nota3 = 8.0, peso1 = 5, peso2 = 3, peso3\r\n"
				+ "= 2. Média: 8.25";

		System.out.println(s + "\n");

		double media, nota1, peso1, nota2, peso2, nota3, peso3;

		System.out.println("Informe a NOTA 1: ");
		nota1 = leitor.nextDouble();

		System.out.println("Informe o PESO 1: ");
		peso1 = leitor.nextDouble();

		System.out.println("Informe a NOTA 2: ");
		nota2 = leitor.nextDouble();

		System.out.println("Informe o PESO 2: ");
		peso2 = leitor.nextDouble();

		System.out.println("Informe a NOTA 3: ");
		nota3 = leitor.nextDouble();

		System.out.println("Informe o PESO 3: ");
		peso3 = leitor.nextDouble();

		media = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

		System.out.printf("A média ponderada é: %.2f", media);

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}
}