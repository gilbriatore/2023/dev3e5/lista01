package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio11 {

	public static void executar(Scanner leitor) {

		String s = "Ex 11. Escreva um programa que leia três valores\r\n"
				+ "inteiros distintos e escreva-os em ordem crescente.";

		System.out.println(s + "\n");

		int num1, num2, num3;

		System.out.println("Informe o primeiro valor: ");
		num1 = leitor.nextInt();

		System.out.println("Informe o segundo valor: ");
		num2 = leitor.nextInt();

		System.out.println("Informe o terceiro valor: ");
		num3 = leitor.nextInt();

		if (num1 <= num2 && num1 <= num3) {
			if (num2 <= num3) {
				System.out.printf("%d, %d, %d", num1, num2, num3);
			} else {
				System.out.printf("%d, %d, %d", num1, num3, num2);
			}
		} else if (num2 <= num1 && num2 <= num3) {
			if (num1 <= num3) {
				System.out.printf("%d, %d, %d", num2, num1, num3);
			} else {
				System.out.printf("%d, %d, %d", num2, num3, num1);
			}
		} else {
			if (num1 <= num2) {
				System.out.printf("%d, %d, %d", num3, num1, num2);
			} else {
				System.out.printf("%d, %d, %d", num3, num2, num1);
			}
		}

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}

	}
}