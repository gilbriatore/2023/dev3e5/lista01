package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio04 {

	public static void executar(Scanner leitor) {

		String s = "Ex 4. Escreva um programa que leia dois números e ao final mostre\r\n"
				+ "a soma, subtração, multiplicação e a divisão dos números lidos.";

		System.out.println(s + "\n");

		float num1, num2, r1, r2, r3, r4;
		System.out.println("Digite o primeiro número: ");
		num1 = leitor.nextFloat();

		System.out.println("Digite o segundo número: ");
		num2 = leitor.nextFloat();

		r1 = num1 + num2;
		r2 = num1 - num2;
		r3 = num1 * num2;
		r4 = num1 / num2;

		System.out.printf("Adição: %.2f\n", r1);
		System.out.printf("Subtração: %.2f\n", r2);
		System.out.printf("Multiplicação: %.2f\n", r3);
		System.out.printf("Divisão: %.2f\n", r4);

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}

	}
}