package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio09 {

	public static void executar(Scanner leitor) {

		String s = "Ex 9. Leia dois números nas variáveis A e B e identifique se os valores\r\n"
				+ "são iguais ou diferentes. Caso eles sejam iguais imprima uma mensagem dizendo\r\n"
				+ "que são iguais. Caso sejam diferentes, informe que são diferentes e\r\n" + "qual número é o maior.";

		System.out.println(s + "\n");

		int a, b;

		System.out.println("Informe o número A: ");
		a = leitor.nextInt();

		System.out.println("Informe o número B: ");
		b = leitor.nextInt();

		if (a == b) {
			System.out.println("Os números são iguais!");
		} else {
			System.out.print("Os números são diferentes e ");
			if (a > b) {
				System.out.println("A é maior do que B!");
			} else {
				System.out.println("B é maior do que A!");
			}
		}

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}
}