package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio15 {

	public static void executar(Scanner leitor) {

		String s = "Ex 15. Tendo como dados de entrada dois pontos quaisquer no plano, P1(x1, y1)\r\n"
				+ "e P2(x2,y2), calcule e retorne a distância entre eles.\r\n\r\n"
				+ "A fórmula que efetua tal cálculo é:\r\n\r\n"
				+ "d = raiz ( ( ( x2 - x1 ) ^ 2 ) + ( ( y2 - y1 ) ^ 2 ) )\r\n\r\n"
				+ "Exemplo: p1(0, 5), p2(10, 20). Distância: 18,03";

		System.out.println(s + "\n");

		double d, x1, x2, y1, y2;

		System.out.println("Informe a coordenada x':");
		x1 = leitor.nextInt();

		System.out.println("Informe a coordenada x'':");
		x2 = leitor.nextInt();

		System.out.println("Informe a coordenada y':");
		y1 = leitor.nextInt();

		System.out.println("Informe a coordenada y'':");
		y2 = leitor.nextInt();

		// Math.sqrt(); -> extrai a raiz quadrada.
		// Math.pow(valor,2); -> faz a elevação do valor ao quadrado.

		d = Math.sqrt((Math.pow((x2 - x1), 2)) + (Math.pow((y2 - y1), 2)));

		System.out.printf("Distância: %.2f", d);

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}
}