package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio10 {

	public static void executar(Scanner leitor) {

		String s = "Ex 10. Escreva um programa que leia um número de 1 a 5 e escreva-o por\r\n"
				+ "extenso. Caso o usuário digite um valor que não esteja neste intervalo,\r\n"
				+ "exibir a mensagem: Número inválido!";

		System.out.println(s + "\n");

		int numero;

		System.out.println("Informe um número: ");
		numero = leitor.nextInt();

		System.out.print("O número digitado é: ");
		switch (numero) {
		case 1:
			System.out.print("UM");
			break;
		case 2:
			System.out.print("DOIS");
			break;
		case 3:
			System.out.print("TRÊS");
			break;
		case 4:
			System.out.print("QUATRO");
			break;
		case 5:
			System.out.print("CINCO");
			break;
		default:
			System.out.println("INVÁLIDO");
		}

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}

	}
}