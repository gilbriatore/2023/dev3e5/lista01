package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio19 {

	public static void executar(Scanner leitor) {

		String s = "Ex 19. Faça um algoritmo que receba o raio e a altura de um cilindro e\r\n"
				+ "retorne o seu volume calculado de acordo com a seguinte fórmula: volume =\r\n"
				+ "3.14 * raio2 * altura; Exemplo: raio = 10, altura = 15. Volume = 4710";

		System.out.println(s + "\n");

		double volume, raio, altura;

		System.out.println("Informe o raio: ");
		raio = leitor.nextDouble();

		System.out.println("Informe a altura: ");
		altura = leitor.nextDouble();

		// Math.pow(raio, 2) eleva o raio ao quadrado;
		volume = 3.14 * Math.pow(raio, 2) * altura;

		System.out.printf("O volume é: %.2f", volume);

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}
}